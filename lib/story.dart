class Story{

  String storyTitle;
  String choice1;
  int choice1NextStory;
  String choice2;
  int choice2NextStory;

  Story({this.storyTitle,this.choice1,this.choice2,this.choice1NextStory,this.choice2NextStory});

}